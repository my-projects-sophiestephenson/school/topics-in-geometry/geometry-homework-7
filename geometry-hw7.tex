\documentclass[12pt, twoside]{amsart}

\newcommand{\R}{{\mathbb R}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\bigH}{{\mathbb H}}
\newcommand{\Rbar}{\overline{\R}}
\newcommand{\xn}{{\vec x_n}}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}

\everymath{\displaystyle}
\usepackage{commath}
\usepackage{pgfplots}
\pgfplotsset{compat=newest, width=10cm}


\begin{document}


\centerline{\large \sc Topics in Geometry - Homework 7}
\centerline{\large  Sophie Stephenson \ December 13, 2019}
\bigskip

\begin{enumerate}


\item[1.)] This problem comes from the proof of the Gauss Bonnet theorem - that the area of a hyperbolic triangle is $\pi$ minus the sum of its interior angles. So, as in the proof, let $T$ be a triangle with one vertex at infinity and its other two vertices on the unit circle at $z = e^{i\theta}$ and $w = e^{i\phi}$. Assume that $0< \theta < \phi$. Show that the interior angle of $T$ at $z$ is $\theta$ and $\pi - \phi$ at $w$.\\
\begin{proof} Consider the figure below.

\begin{center}
\begin{tikzpicture}
       \begin{axis}[
       		axis lines = left,
       		xlabel = {$Re(z)$},
		ylabel = {$Im(z)$},
		xmin = -2, xmax = 1.5, ymin = 0, ymax = 3
       ]
       \draw (0, 0) -- (0, 4);
       % unit circle
       \draw[thick] (30:1) arc (30:130:1);
       \draw[loosely dashed] (0:1) arc (0:30:1);
       \draw[dashed] (130:1) arc (130:180:1);
       % rest of T
       \draw[thick] (0.866, 0.5) -- (0.866, 4);  % z
       \draw[thick] (-0.643, 0.766) -- (-0.643, 4); % w
       % lines through z and w
       \draw[color=red, dashed] (0, 0) --  (1.732, 1);  
       \draw[color=blue, dashed] (0, 0) -- (-1.286, 1.532); 
       % perpendicular bisectors
       \draw[color=red, dashed] (0, 2) -- (1.155, 0); 
       \draw [color=blue, dashed] (0, 1.306) -- (-1.55, 0); 
       
       % z labels
       \node [below right] at (axis cs: 0.866, 0.56) {$z$};
       \node [right] at (axis cs: 0.29, 0.12) {$\theta$};
       \node at (axis cs: 0.16, 0.28) {$\beta$};
       \node at (axis cs: 0.1, 1.6) {$\gamma$};
       \node at (axis cs: 0.78, 0.86) {$\delta$};
       % w labels
       \node [left] at (axis cs: -0.643, 0.766) {$w$};
       \node [left] at (axis cs: -0.2, 0.17) {$\pi - \phi$};
       \node at (axis cs: -0.12, 0.4) {$\beta'$};
       \node at (axis cs: -0.12, 1.05) {$\gamma'$};
       \node at (axis cs: -0.5, 1) {$\delta '$};

       \end{axis} 
     \end{tikzpicture}
\end{center}
We will begin with the interior angle at $z$. After drawing the radius of the unit circle with endpoint at $z$ and the perpendicular bisector of that radius (in red on the graph), we see that the angle we need is angle $\delta$. First, note that $\theta + \beta = \pi/2$, we have $\beta = \pi/2 - \theta$. Then, since the red lines are perpendicular, $\gamma = \pi/2 - \beta = \theta$. Finally, because the right side of the triangle and the imaginary axis are parallel, we must have that $\gamma = \delta = \theta$, so the interior angle at $z$ is $\theta$, as desired.\\

Next, we will show that the interior angle at $w$ is $\pi - \phi$. We again draw the radius of the unit circle, this time with endpoint at $w$, and the perpendicular bisector of that radius (shown in blue on the graph). The angle we need to find is $\delta'$. First, since $\pi - \phi + \beta' = \pi/2$, we have that $\beta' = \phi - \pi/2$. Next, again since the two blue lines are perpendicular, we see that $\gamma' = \pi/2 - (\phi - \pi/2) = \pi - \phi$. Finally, because the left side of the triangle and the imaginary axis are parallel, $\gamma' = \delta' = \phi - \pi$, so the interior angle at $w$ is $\pi - \phi$, as desired. \\

If $Re(z) > 0$ and $Re(w) > 0$, we can derive the interior angle at $w$ as follows (the angle at $z$ can be found precisely as above):

\begin{center}
\begin{tikzpicture}
       \begin{axis}[
       		axis lines = left,
       		xlabel = {$Re(z)$},
		ylabel = {$Im(z)$},
		xmin = 0, xmax = 1.25, ymin = 0, ymax = 1.25
       ]
       \draw (0, 0) -- (0, 4);
       % unit circle
       \draw[thick] (30:1) arc (30:80:1);
       \draw[loosely dashed] (0:1) arc (0:30:1);
       \draw[dashed] (80:1) arc (80:180:1);
       % rest of T
       \draw[thick] (0.866, 0.5) -- (0.866, 4);  % z
       \draw[thick] (0.174, 0.984) -- (0.174, 4); % w
       % lines through z and w
       \draw[color=blue, dashed] (0, 0) -- (0.348, 1.968); % slope 5.655
       % perpendicular bisector
       \draw [color=blue, dashed] (0, 1.015) -- (5.734, 0); % slope -0.177
       % labels
       \node [below right] at (axis cs: 0.866, 0.56) {$z$};
       \node [left] at (axis cs: 0.24, 0.93) {$w$};
       \node at (axis cs: 0.06, 0.06) {$\phi$};
       \node [left] at (axis cs: 0.08, 0.46) {$\alpha$};
       \node at (axis cs: 0.05, 0.94) {$\beta$};
       \node at (axis cs: 0.14, 1.05) {$\gamma$};
       \node at (axis cs: 0.25, 1.03) {$\delta$};

       \end{axis} 
     \end{tikzpicture}
\end{center}

First, note that $\alpha = \pi/2 - \phi$. Because the two blue lines are perpendicular, we derive that $\beta = \pi/2 - \alpha = \phi$. Next, since the vertical line with endpoint $w$ is parallel to the imaginary axis, $\beta = \gamma = \phi$. Finally, $\delta$ is complementary to $\gamma$, so $\delta = \pi - \phi$, as desired.\\

If $Re(z) < 0$ and $Re(w) < 0$, we find the interior angle at $z$ similarly to the above, and we find $w$ precisely as in the first figure. In any case, we can derive that the interior angle at $z$ is $\theta$ and the interior angle at $w$ is $\pi - \phi$.\\
\end{proof}
\bigskip

\item[2.)] Let $S$ be a hyperbolic quadrilateral, i.e. a four sided figure whose sides are geodesic arcs. Prove that the area of $S, \mu_{\bigH}(S) = 2\pi - (\alpha + \beta + \gamma + \delta)$, where $\alpha, \beta, \gamma, \delta$ are the interior angles at each of the four vertices of $S$. To do this, partition $S$ into triangles and use the Gauss Bonnet theorem for triangles.\\
\begin{proof} Assume that $S$ has vertices $p_1, p_2, p_3,$ and $p_4$, where the interior angle at $p_1$ is $\alpha$, the interior angle at $p_2$ is $\beta$, the angle at $p_3$ is $\gamma$, and the angle at $p_4$ is $\delta$. Partition $S$ into two triangles $T_1$ and $T_2$, where $T_1$ has vertices $p_1, p_2,$ and $p_4$, and $T_2$ has vertices $p_2, p_3$, and $p_4$. We see that the interior angles of $T_1$ are $\alpha, \delta_1$, and $\beta_1$, and the interior angles of $T_2$ are $\gamma, \delta_2$, and $\beta_2$, where $\delta_1 + \delta_2 = \delta$ and $\beta_1 + \beta_2 = \beta$.\\

Now, notice that $\mu_{\bigH}(S) = \mu_{\bigH}(T_1) + \mu_{\bigH}(T_2)$. By the Gauss Bonnet theorem, we have that $ \mu_{\bigH}(T_1) = \pi - (\alpha + \delta_1 + \beta_1)$ and  $\mu_{\bigH}(T_2) = \pi - (\gamma + \delta_2 + \beta_2)$. Thus, 
$$\mu_{\bigH}(S) = \pi - (\alpha + \delta_1 + \beta_1) + \pi - (\gamma + \delta_2 + \beta_2) = 2\pi - (\alpha + \beta + \gamma + \delta),$$
as desired.
\end{proof}
\bigskip

\item[3.)] Let $M, N, M' \in SL(2, \R)$ where $SL(2, \R)$ is the set of 2 $\times$ 2 real matrices with determinant one. Prove that if $M' = NMN^{-1}$, then the trace of $M$ is equal to the trace of $M'$. This says the trace of a matrix is conjugacy invariant. This result implies that if a transformation $T$ is conjugate to a transformation $T'$, then $T$ and $T'$ have the same type (i.e., both are loxodromic, both are elliptic or both are parabolic).\\
\begin{proof} Assume that $M = \begin{pmatrix} a & b \\ c & d \end{pmatrix}$ and $N = \begin{pmatrix} u & v \\ x & y \end{pmatrix}$. Since the determinant of $M$ and $N$ is 1, we have that 
\begin{align*}
NMN^{-1} &= \begin{pmatrix} u & v \\ x & y \end{pmatrix}\begin{pmatrix} a & b \\ c & d \end{pmatrix}\begin{pmatrix} y & -v \\ -x & u \end{pmatrix}\\
&= \begin{pmatrix} ua + vc & ub + vd \\ xa + yc & xb + yd \end{pmatrix}\begin{pmatrix} y & -v \\ -x & u \end{pmatrix} \\
&= \begin{pmatrix} y(au + cv) -x(bu + dv) & -v(au +cv) + u(bu + dv) \\ y(ax + cy) - x(bx + dy) & -v(ax +cy) + u(bx + dy) \end{pmatrix}.
\end{align*}
We will show that $Tr(M) = Tr(NMN^{-1})$. Calculating the trace of $NMN^{-1}$: 
\begin{align*}
Tr(NMN^{-1}) &= y(au + cv) -x(bu + dv) - v(ax +cy) + u(bx + dy)\\
&= yau + ycv - xbu - xdv - vax - vcy + ubx + udy\\
&= a(uy - vx) + d(uy - vx)\\
&= a + d\\
&= Tr(M),
\end{align*}
since $uy - vx = 1$. Thus $Tr(M) = Tr(NMN^{-1}$, as desired.\\
\end{proof}
\bigskip

\item[4.)] I claimed in class that the transformation $T(z) = \tfrac{kz + 0}{0z+1/k}$ was loxodromic. Show that $Tr(T) = |k + \tfrac{1}{k}| > 2$ as long as $k \not = 1$. (If $k = 1$, this is just the identity transformation, which we don't think of as having a type.)
\begin{proof} For all $k \not = 1$, we have that $(k - 1)^2 > 0$. Thus, we can say
\begin{align*}
	(k - 1)^2 > 0 &\implies k^2 -2k + 1 > 0\\
		&\implies k^2 + 1 > 2k\\
		&\implies \abs{\tfrac{k^2 + 1}{k}} > 2\\
		&\implies |k + \tfrac{1}{k}| > 2,
\end{align*}	
	for $k \not = 1$, as desired.

\end{proof}
\bigskip

\item[5.)] Classify each of the following types of isometries by finding their fixed points. If elliptic, find its center of rotation, if loxodromic find its axis of translation. (It suffices to give the endpoints of the axis since they determine a unique hyperbolic line.)\\
\begin{enumerate}
	\item $T(z) = 7z + 6$
	\begin{proof}[Solution] Taking $T(z) = 7z + 6 = z$, we find that the fixed points of $T$ are $\infty$ and $-1$. Since $T$ has two fixed points in $\Rbar$, $T$ is loxodromic. Its axis of translation is the line $Re(z) = 1$. \\
	\end{proof}
	
	\item $T(z) = \tfrac{z}{z + 1}$
	\begin{proof}[Solution] Solving $T(z) = \tfrac{z}{z + 1} = z$ gives us one fixed point, 0. Since $T$ has one fixed point in $\Rbar$, $T$ is parabolic. \\
	\end{proof}
	
	\item $T(z) = \tfrac{-1}{z}$
	\begin{proof}[Solution] The only fixed point of $T(z) = \frac{-1}{z}$ is $i$ (or $-i$, but we don't care about that). This is one fixed point in $\bigH$, so $T$ must be elliptic, and its center of rotation is $i$.
	\end{proof}
	
\end{enumerate}
\bigskip



\end{enumerate}


\end{document}